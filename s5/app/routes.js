const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
    app.get('/', (req, res) => {
        return res.send({ 'data': {} });
    });

    app.get('/rates', (req, res) => {
        return res.send({
            rates: exchangeRates
        });
    });

    app.post('/currency', (req, res) => {

    	const duplicateAlias = Object.values(exchangeRates).find((currency) => currency.alias === req.body.alias);

        if (!req.body.hasOwnProperty('name') || !req.body.hasOwnProperty('alias') || !req.body.hasOwnProperty('ex')) {
            return res.status(400).send({
                'error': 'Bad Request - missing required parameters: name, alias, or ex'
            });
        }

        if (typeof req.body.name !== 'string' || req.body.name.trim() === '') {
            return res.status(400).send({
                'error': 'Bad Request - invalid parameter: name must be a non-empty string'
            });
        }

        if (typeof req.body.alias !== 'string' || req.body.alias.trim() === '') {
            return res.status(400).send({
                'error': 'Bad Request - invalid parameter: alias must be a non-empty string'
            });
        }

        if (typeof req.body.ex !== 'object') {
            return res.status(400).send({
                'error': 'Bad Request - invalid parameter: ex must be an object'
            });
        }

        if (Object.keys(req.body.ex).length === 0) {
            return res.status(400).send({
                'error': 'Bad Request - ex object must not be empty'
            });
        }

        if (duplicateAlias) {
            return res.status(400).send({
                'error': 'Bad Request - alias must be unique'
            });
        }

        return res.status(200).send({
            'message': 'Complete'
        });
    });
};
