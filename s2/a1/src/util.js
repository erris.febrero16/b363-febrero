function factorial(n){
	if(n===0) return 1;
	if(n===1) return 1;
	return n * factorial(n-1);
}

function div_check(num) {
  if (num % 5 === 0 || num % 7 === 0) 
  return true;
}

module.exports = {factorial, div_check}