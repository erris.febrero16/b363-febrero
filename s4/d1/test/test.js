const { factorial, div_check } = require('../src/util.js');

// Gets the expect and assert functions from chai to be used
const {expect, assert} = require('chai');

// Test Suites are made up of collection of test cases that should be executed together

// "describe()" keyword is used to group tests together
describe('test_fun_factorials', () => {

	// "it()" accepts two parameters
	// string explaining what the test should do
	// callback function which contains the actual test
	it('test_fun_factorial_5!_is_120', () => {
		const product = factorial(5);
		// "expect" - returning expected and actual value
		expect(product).to.equal(120);
	})

	// it('test_fun_factorial_5!_is_120', () => {
	// 	const product = factorial(5);
	// 	assert.equal(product, 120)
	// })

	it('test_fun_factorial_1!_is_1', () => {
		const product = factorial(1);
		// "assert" - checking if function is return correct results
		assert.equal(product, 1);
	})

	// it('test_fun_factorial_1!_is_1', () => {
	// 	const product = factorial(1);
	// 	expect(product).to.equal(1);
	// })

	// 1. In your test.js, create test cases in the test fun factorials test suite to check if the factorial function's result for 0!, 4!, and 10! using assert and/or expect.
	it('test_fun_factorial_0!_is_1', () => {
	    const product = factorial(0)
	    assert.equal(product, 1)
	});

	it('test_fun_factorial_4!_is_24', () => {
	    const product = factorial(4)
	    expect(product).to.equal(24)
	});

	it('test_fun_factorial_10!_is_3628800', () => {
	    const product = factorial(10)
	    expect(product).to.equal(3628800)
	})

	// Test for negative numbers
	it("test_fun_factorial_neg_1_is_undefined", () => {
		// RED - Write a test that fails
		const product = factorial(-1);
		expect(product).to.equal(undefined);
	})

	// Mini-activity
	/*
	Create a failing test to test a scenario if a user enters data that is not a number.
	- Expect the product to be undefined.
	- test_fun_factorials_invalid_number_is_undefined
	Add condition in util.js
	Refactor the code, add factorial
	*/

	it("test_fun_factorials_invalid_number_is_undefined", () => {
		const product = factorial("abc");
		expect(product).to.equal(undefined);
	})

})

// 3. Create 4 test cases in a new test suite in test.js that would check if the functionality of div_check is correct.
describe('test_divisibilty_by_5_or_7', ()=>{

    it('test_100_is_divisible_by_5', () => {
        const number = div_check(100);
        expect(number).to.equal(true);
    })

    it('test_49_is_divisible_by_7', () => {
        const number = div_check(49);
        expect(number).to.equal(true);
    })  


    it('test_30_is_divisible_by_5', () => {
        const number = div_check(30);
        expect(number).to.equal(true);
    })

    it('test_56_is_divisible_by_7', () => {
        const number = div_check(56);
        expect(number).to.equal(true);
    })  

})
